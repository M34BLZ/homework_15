// Homework15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

const int N_even = 0; //четные
const int N_odd = 1; //нечетные

//функция выдаёт по флагу Flg чётные или нечётные числа от 0 до N. Flg = true :чётные числа
void OutNumbers2(int N, int Flg)
{
    for (int i = 0; i < N; ++i)
    {
        int id = Flg + ( i % 2);
        switch (id)
        {
        case 2:  std::cout << i << "\n";
                break;
            
        case 0: std::cout << i << "\n";
        }

    }

}


int main()
{
    const int N = 19;
    for (int i = 0; i < N; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }

    std::cout << "==============Function============\n";
     OutNumbers2(19, N_even);

    std::cout << "==============Function============\n";
     OutNumbers2(19, N_odd);

}
